#include "catch.hpp"

#include "libcubemars_can/can_interface.h"

using namespace libcubemars_can;

TEST_CASE("CanInterface Constructor", "CanInterface") {

    auto fout = CanInterface::Create( "can0" );

    CHECK( (bool)fout );

}
