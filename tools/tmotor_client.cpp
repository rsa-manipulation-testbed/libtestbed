// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo
//
// A simple command line client for running a TMotor in position mode

#include <cmath>
#include <csignal>
#include <functional>
#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "libcubemars_can/can_interface.h"
#include "libcubemars_can/tmotor.h"
#include "libcubemars_can/tmotor_constants.h"

#include "kb_nonblocking.h"

using namespace libcubemars_can;

// int canId = 1;

namespace {
std::function<void(int)> signal_function;
void signal_handler(int signal) { signal_function(signal); }
} // namespace

class TMotorClient {
public:
  TMotorClient(CanInterface canif, int canId, TMotorConstants constants)
      : _canIf(canif), _canId(canId), _cmd(constants), _initialized(false),
        _done(false) {

    // Signal can't take a lambda directly, but the global "signal_function" can
    signal(SIGINT, signal_handler);
    signal_function = [&](int signum) {
      if (signum == SIGINT) {
        _done = true;
        return;
      }

      exit(signum);
    };
  }

  void read_reply(CanInterface &can) {
    struct can_frame frame;

    if (can.receive(frame)) {
      auto state = TMotor::unpackFrame(frame, _cmd.constants);

      if (state.id == _canId) {
        std::cout << "----" << std::endl;
        state.dump();
        std::cout << "----" << std::endl;

        if (!_initialized) {
          std::cerr << "Initializing driver with position: " << state.pos
                    << std::endl;

          _cmd.setPos(state.pos).setKd(kd).setKp(kp);
          _initialized = true;
        }
      } else {
        std::cerr << "Got bad id " << state.id << std::endl;
      }
    }
  }

  void run() {
    _cmd.setPos(0).setVel(0).setTorque(0).setKp(0).setKd(0);

    std::cout << "Using constants for: " << _cmd.constants.name << std::endl;
    std::cout << "Press < and > to shift motor position." << std::endl;
    std::cout << "Press Q to quit" << std::endl;
    std::cout << "Pres [enter] to enable motor" << std::endl;
    std::string input;
    std::cin.ignore();

    /* send frame */
    struct can_frame txFrame = TMotor::enableCommand(_canId);
    std::cout << "Enabling motor" << std::endl;
    if (_canIf.send(txFrame) < 0) {
      std::cerr << "Error sending motor start" << std::endl;
      return;
    }

    read_reply(_canIf);

    while (!_done) {
      txFrame = TMotor::motorCommand(_canId, _cmd);
      _cmd.dump();
      if (_canIf.send(txFrame) < 0) {
        std::cerr << "Error sending motor command (" << errno
                  << "): " << strerror(errno) << std::endl;
        break;
      }

      read_reply(_canIf);

      const float delta = 0.5;

      if (kbhit() != 0) {
        auto ch = getchar();
        std::cout << "Got: " << ch << std::endl;
        if ((ch > 0) && (isprint(ch))) {
          if (toupper(ch) == '<') {
            _cmd.setPos(_cmd.pos() - delta);
            std::cout << "NEW POSITION: " << _cmd.pos() << std::endl;
          } else if (toupper(ch) == '>') {
            _cmd.setPos(_cmd.pos() + delta);
            std::cout << "NEW POSITION: " << _cmd.pos() << std::endl;
          } else if (toupper(ch) == 'Q') {
            _done = true;
          }
        }
      }

      usleep(100000);
    }

    txFrame = TMotor::disableCommand(_canId);
    std::cout << "Disabling motor" << std::endl;
    if (_canIf.send(txFrame) < 0) {
      std::cerr << "Error sending motor stop" << std::endl;
      return;
    }
    read_reply(_canIf);
  }

  CanInterface _canIf;
  int _canId;
  TMotor::MotorCommand _cmd;

  bool _initialized;

  bool _done;

  const float kp = 10.0;
  const float kd = 2.5;
};

// const TMotorConstants constants = TMotorConstants::AK80_6_V2_Constants;
// TMotor::MotorCommand cmd(constants);

int main(int argc, char **argv) {
  nonblock(true);

  int canId = 1;
  std::string canInterface = "can0";
  TMotorConstants constants = TMotorConstants::AK70_10_V2_Constants;

  try {
    // Parse command line options
    // clang-format off
    po::options_description options("Options");
    options.add_options()
        ("help", "produce help message")
        ("canif", po::value<std::string>())
        ("id", po::value<int>());
    // clang-format on

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      exit(0);
    }

    if (vm.count("canif")) {
      canInterface = vm["canif"].as<std::string>();
    }

    if (vm.count("id")) {
      canId = vm["id"].as<int>();
    }

  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    exit(-1);
  }

  auto canF = CanInterface::Create(canInterface);
  if (!canF) {
    std::cerr << "Error creating CAN interface: " << canF.error().msg()
              << std::endl;
    exit(-1);
  }

  std::cout << "Opening CAN interface " << canInterface << std::endl;
  std::cout << " Connecting to CAN ID " << canId << std::endl;

  auto canIf(canF.value());
  TMotorClient tmotor(canIf, canId, constants);

  tmotor.run();

  nonblock(false);

  exit(0);
}
