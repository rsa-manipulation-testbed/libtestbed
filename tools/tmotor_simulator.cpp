//
// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <cmath>
#include <csignal>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include "libcubemars_can/tmotor.h"
#include "libcubemars_can/can_interface.h"

#include <iostream>


using namespace libcubemars_can;

bool isDone = false;

const TMotorConstants constants = TMotorConstants::AK80_9_Constants;

void signalHandler(int signum) {
    if (signum == SIGINT) {
        isDone = true;
        return;
    }
    exit(signum);
}

int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " [id]" << std::endl;
        return -1;
    }

    // \todo Get the following from the command line
    const unsigned int motorId = atoi(argv[1]);
    std::cout << "Starting with CAN ID " << motorId << std::endl;

    const int masterId = 0;

    const std::string devname = "vcan0";

    signal(SIGINT, signalHandler);

    auto canF = CanInterface::Create(devname);
    if (!canF) {
        std::cerr << "Error creating CAN interface: " << canF.error().msg()
                  << std::endl;
        exit(-1);
    }

    auto canIf = canF.value();

    TMotor::MotorState state;

    while (!isDone) {
        struct can_frame frame;

        if (canIf.receive(frame) > 0) {
            std::cerr << "Got msg for " << frame.can_id << std::endl;

            if (frame.can_id !=  motorId) continue;

            frame.can_dlc = 6;
            state.pack(motorId, frame.data, constants);

            std::cerr << "Replying" << std::endl;
            frame.can_id = masterId;
            canIf.send(frame);
        }
    }

    return 0;
}
