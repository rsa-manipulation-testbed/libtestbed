// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo
//
// A simple command line client for running a TMotor in position mode

#include <cmath>
#include <csignal>
#include <iostream>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <termios.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include "libcubemars_can/can_interface.h"
#include "libcubemars_can/tmotor.h"
#include "libcubemars_can/tmotor_constants.h"

#include "kb_nonblocking.h"

using namespace libcubemars_can;

bool isDone = false;

const TMotorConstants constants = TMotorConstants::AK80_6_V2_Constants;
TMotor::MotorCommand cmd(constants);

bool initialized = false;
int canId = 1;

const float kp = 100.0;
const float kd = 2.5;

void signalHandler(int signum) {
  if (signum == SIGINT) {
    isDone = true;
    return;
  }

  exit(signum);
}

void read_reply(CanInterface &can) {
  struct can_frame frame;

  if (can.receive(frame)) {
    auto state = TMotor::unpackFrame(frame, constants);

    if (state.id == canId) {
      std::cout << "----" << std::endl;
      state.dump();
      std::cout << "----" << std::endl;

      if (!initialized) {
        std::cerr << "Initializing driver with position: " << state.pos
                  << std::endl;

        cmd.setPos(state.pos).setKd(kd).setKp(kp);
        initialized = true;
      }
    } else {
      std::cerr << "Got bad id " << state.id << std::endl;
    }
  }
}

int main(int argc, char **argv) {
  unsigned char ch;
  nonblock(true);

  signal(SIGINT, signalHandler);

  std::string canInterface = "can0";

  try {
    // Parse command line options
    po::options_description options("Options");
    options.add_options()("help", "produce help message")(
        "canif", po::value<std::string>(&canInterface))("id",
                                                        po::value<int>(&canId));

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      return -1;
    }

  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    return -1;
  }

  auto canF = CanInterface::Create(canInterface);
  if (!canF) {
    std::cerr << "Error creating CAN interface: " << canF.error().msg()
              << std::endl;
    exit(-1);
  }

  auto canif(canF.value());

  cmd.setPos(0).setVel(0).setTorque(0).setKp(0).setKd(0);

  std::cout << "Using constants for: " << constants.name << std::endl;
  std::cout << "Press < and > to shift motor position." << std::endl;
  std::cout << "Press Q to quit" << std::endl;
  std::cout << "Pres [enter] to enable motor" << std::endl;
  std::string input;
  std::cin.ignore();

  /* send frame */
  struct can_frame txFrame = TMotor::enableCommand(canId);
  std::cout << "Enabling motor" << std::endl;
  if (canif.send(txFrame) < 0) {
    std::cerr << "Error sending motor start" << std::endl;
    exit(-1);
  }

  read_reply(canif);

  while (!isDone) {
    txFrame = TMotor::motorCommand(canId, cmd);
    cmd.dump();
    if (canif.send(txFrame) < 0) {
      std::cerr << "Error sending motor command (" << errno
                << "): " << strerror(errno) << std::endl;
      break;
    }

    read_reply(canif);

    const float delta = 0.5;

    if (kbhit() != 0) {
      ch = getchar();
      std::cout << "Got: " << ch << std::endl;
      if ((ch > 0) && (isprint(ch))) {
        if (toupper(ch) == '<') {
          cmd.setPos(cmd.pos() - delta);
          std::cout << "NEW POSITION: " << cmd.pos() << std::endl;
        } else if (toupper(ch) == '>') {
          cmd.setPos(cmd.pos() + delta);
          std::cout << "NEW POSITION: " << cmd.pos() << std::endl;
        } else if (toupper(ch) == 'Q') {
          isDone = true;
        }
      }
    }

    usleep(100000);
  }

  txFrame = TMotor::disableCommand(canId);
  std::cout << "Disabling motor" << std::endl;
  if (canif.send(txFrame) < 0) {
    std::cerr << "Error sending motor stop" << std::endl;
    exit(-1);
  }
  read_reply(canif);

  nonblock(false);

  return 0;
}
