//
// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#pragma once

#include <unistd.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <string>

#include <linux/can.h>
#include <linux/can/raw.h>

#include <sstream>

#include "libcubemars_can/expected.hpp"

namespace libcubemars_can {

/// Implements a simple, synchronous can interface for communication with
/// one or more TMotors.
class CanInterface {
 public:
    //== Factory functions ==
    // Typedef for the factory return values
    struct FactoryError {
        explicit FactoryError(const std::string &msg, int errnum = -1)
            : err(errnum), _msgstr(msg)
            {;}

        explicit FactoryError(int errnum = -1)
            : err(errnum), _msgstr()
            {;}

        // Custom copy constructor
        FactoryError(const FactoryError &other)
            : err(other.err),
            _msgstr(other._msgstr.str())
            {;}

        std::stringstream &stream()     { return _msgstr; }
        const std::string msg() const   { return _msgstr.str(); }

        int err;
        std::stringstream _msgstr;
    };

    typedef tl::expected< CanInterface, FactoryError > FactoryExpected;

    // Factory for creating a new camera instance.
    static FactoryExpected Create(const std::string &device);

    CanInterface() = delete;
    CanInterface(const CanInterface &other)
        : _fd(dup(other._fd))
        {;}

    ~CanInterface();


    int send(const struct can_frame &frame);
    int receive(struct can_frame &frame);

    int sendReceive(const struct can_frame &tx, struct can_frame &rx);

 protected:
    explicit CanInterface(int fd);

    int _fd;
};

}  // namespace libcubemars_can
