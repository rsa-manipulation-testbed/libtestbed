//
// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#pragma once

#include "libcubemars_can/tmotor_helpers.h"
#include <assert.h>
#include <string>

const int P_BITS = 16;
const int V_BITS = 12;
const int T_BITS = 12;
const int Kp_BITS = 12;
const int Kd_BITS = 12;

const unsigned int V1_PROTOCOL_REPLY_LENGTH = 6;
const unsigned int V2_PROTOCOL_REPLY_LENGTH = 8;

namespace libcubemars_can {

struct TMotorConstants {
public:
  struct MinMax {
    float min, max;
  };

  // P is in radians
  // V is in rad/sec
  // T is in N-m
  MinMax P, V, T, Kp, Kd;
  std::string name;

  enum TMotorProtocol_t { PROTOCOL_V1 = 1, PROTOCOL_V2 = 2 } protocol;

  static const TMotorConstants AK80_9_Constants;
  static const TMotorConstants AK80_6_Constants;
  static const TMotorConstants AK80_6_V2_Constants;
  static const TMotorConstants AK80_9_V2_Constants;
  static const TMotorConstants AK70_10_V2_Constants;

  int P_to_uint(const float p) const {
    return float_to_uint(p, P.min, P.max, P_BITS);
  }

  int V_to_uint(const float v) const {
    return float_to_uint(v, V.min, V.max, V_BITS);
  }

  int T_to_uint(const float t) const {
    return float_to_uint(t, T.min, T.max, T_BITS);
  }

  int Kp_to_uint(const float kp) const {
    return float_to_uint(kp, Kp.min, Kp.max, Kp_BITS);
  }

  int Kd_to_uint(const float kd) const {
    return float_to_uint(kd, Kd.min, Kd.max, Kd_BITS);
  }

  float P_to_float(unsigned int p) const {
    return uint_to_float(p, P.min, P.max, P_BITS);
  }

  float V_to_float(unsigned int v) const {
    return uint_to_float(v, V.min, V.max, V_BITS);
  }

  float T_to_float(unsigned int i) const {
    return uint_to_float(i, T.min, T.max, T_BITS);
  }

  unsigned int reply_length(void) const {
    if (protocol == PROTOCOL_V1) {
      return V1_PROTOCOL_REPLY_LENGTH;
    } else if (protocol == PROTOCOL_V2) {
      return V2_PROTOCOL_REPLY_LENGTH;
    }

    // Should never get here
    assert(false);
    return 0;
  }
};

} // namespace libcubemars_can
