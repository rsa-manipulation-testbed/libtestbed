#include "libcubemars_can/tmotor_constants.h"

namespace libcubemars_can {

const TMotorConstants TMotorConstants::AK80_9_Constants = {
    .P = {-12.5, 12.5},
    .V = {-25.64, 25.64},
    .T = {-18, 18},
    .Kp = {0, 500},
    .Kd = {0, 5},
    .name = "AK80-9-V1",
    .protocol = PROTOCOL_V1 };

const TMotorConstants TMotorConstants::AK80_6_Constants = {
    .P = {-12.5, 12.5},
    .V = {-38.2, 38.2},
    .T = {-12, 12},
    .Kp = {0, 500},
    .Kd = {0, 5},
    .name ="AK80-6-V1",
    .protocol = PROTOCOL_V1 };

const TMotorConstants TMotorConstants::AK80_6_V2_Constants = {
    .P = {-12.5, 12.5},
    .V = {-38.2, 38.2},
    .T = {-12, 12},
    .Kp = {0, 500},
    .Kd = {0, 5},
    .name ="AK80-6-V2",
    .protocol = PROTOCOL_V2 };

const TMotorConstants TMotorConstants::AK80_9_V2_Constants = {
    .P = {-12.5, 12.5},
    .V = {-25.64, 25.64},
    .T = {-18, 18},
    .Kp = {0, 500},
    .Kd = {0, 5},
    .name = "AK80-9-V2",
    .protocol = PROTOCOL_V2 };

const TMotorConstants TMotorConstants::AK70_10_V2_Constants = {
    .P = {-12.5, 12.5},
    .V = {-50.0, 50.0},
    .T = {-25, 25},
    .Kp = {0, 500},
    .Kd = {0, 5},
    .name ="AK70-10-V2",
    .protocol = PROTOCOL_V2 };

}
