//
// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#include <math.h>
#include <string.h>

#include <iostream>

#include "libcubemars_can/tmotor.h"
#include "libcubemars_can/tmotor_constants.h"
#include "libcubemars_can/tmotor_helpers.h"


namespace libcubemars_can {

namespace TMotor {

//const struct MotorCommand MotorCommand::NoMotionCommand;

void packEnableCommand(unsigned char *data) {
    memset(data, 0xFF, 7);
    data[7] = 0xFC;
}

void packDisableCommand(unsigned char *data) {
    memset(data, 0xFF, 7);
    data[7] = 0xFD;
}

void packZeroPositionCommand(unsigned char *data) {
    memset(data, 0xFF, 7);
    data[7] = 0xFE;
}

struct can_frame initFrame(unsigned int id) {
    struct can_frame frame;
    memset( &frame, 0, sizeof(struct can_frame) );
    frame.can_id = id;
    frame.can_dlc = CommandLength;
    return frame;
}

struct can_frame enableCommand(unsigned int id) {
    auto frame = initFrame(id);
    packEnableCommand(frame.data);
    return frame;
}

struct can_frame disableCommand(unsigned int id) {
    auto frame = initFrame(id);
    packDisableCommand(frame.data);
    return frame;
}

struct can_frame zeroPositionCommand(unsigned int id) {
    auto frame = initFrame(id);
    packZeroPositionCommand(frame.data);
    return frame;
}

struct can_frame motorCommand(unsigned int id, const MotorCommand &cmd) {
    auto frame = initFrame(id);
    cmd.pack(frame.data);
    return frame;
}

// MotorCommand

MotorCommand &MotorCommand::setPos(float p) {
    _pos = p;
    return *this;
}

MotorCommand &MotorCommand::setVel(float v) {
    _vel = v;
    return *this;
}

MotorCommand &MotorCommand::setTorque(float t) {
    _t_ff = t;
    return *this;
}

MotorCommand &MotorCommand::setKp(float kp) {
    _kp = kp;
    return *this;
}

MotorCommand &MotorCommand::setKd(float kd) {
    _kd = kd;
    return *this;
}

void MotorCommand::pack(unsigned char *buffer) const {

    // Convert floats to unsinged int
    const int p_int  = constants.P_to_uint(_pos);
    const int v_int  = constants.V_to_uint(_vel);
    const int kp_int = constants.Kp_to_uint(_kp);
    const int kd_int = constants.Kd_to_uint(_kd);
    const int t_int  = constants.T_to_uint(_t_ff);

    // Back ints into can buffer
    buffer[0] = p_int >> 8;          // position 8-H
    buffer[1] = p_int&0xFF;        // position 8-L

    buffer[2] = v_int >> 4;          // speed 8H
    buffer[3] = ((v_int&0x0F) << 4) | (kp_int >> 8);   // speed-4L KP-8H
    buffer[4] = kp_int&0xFF;       // KP 8-L
    buffer[5] = kd_int >> 4;         // Kd 8-H
    buffer[6] = ((kd_int&0x0F) << 4) | (t_int >> 8);    // KP 4-L torque 4-H
    buffer[7] = t_int&0xFF;        // torque 8-L
}

void MotorCommand::dump(void) const {
    std::cerr << "CMD: P:   " << _pos << std::endl;
    std::cerr << "CMD: V:   " << _vel << std::endl;
    std::cerr << "CMD: kp:  " << _kp << std::endl;
    std::cerr << "CMD: kd:  " << _kd << std::endl;
    std::cerr << "CMD: Iff: " << _t_ff << std::endl;
}


// MotorState

MotorState unpackFrame(const can_frame &frame, const TMotorConstants &constants) {
    MotorState state;

    if (frame.can_dlc == constants.reply_length()) {
        state.unpack(frame.data, constants);
    } else {
        std::cerr << "Mismatch between CAN frame length and expected length" << std::endl;
    }

    return state;
}

void MotorState::unpack(const unsigned char *buffer, const TMotorConstants &constants) {
    id = buffer[0];        // Driver ID number

    const int p_int = (buffer[1] << 8) | buffer[2];          // Motor position
    const int v_int = (buffer[3] << 4) | buffer[4] >> 4;     // Motor speed
    const int i_int = ((buffer[4]&0x0F) << 8) | buffer[5];   // Motor torque

    pos = constants.P_to_float(p_int);
    vel = constants.V_to_float(v_int);
    current = constants.T_to_float(i_int);

    if (constants.protocol == TMotorConstants::PROTOCOL_V2) {
        temperature = buffer[6];
        error = buffer[7];
    }
}

// Required for simulation
void MotorState::pack(int id, unsigned char *buffer, const TMotorConstants &constants) {
    buffer[0] = id;        // Driver ID number

    const int p_int = constants.P_to_uint(pos);
    const int v_int = constants.V_to_uint(vel);
    const int i_int = constants.T_to_uint(current);

    buffer[1] = (p_int & 0xFF00) >> 8;
    buffer[2] = (p_int & 0x00FF);
    buffer[3] = (v_int & 0x0FF0) >> 4;
    buffer[4] = ((v_int & 0x000F) << 4) | ((i_int & 0x0F00) >> 8);
    buffer[5] = i_int & 0x00FF;

    if ((protocol == TMotorConstants::PROTOCOL_V2) &&
       (constants.protocol == TMotorConstants::PROTOCOL_V2)) {
        buffer[6] = temperature;
        buffer[7] = error;
    }
}

void MotorState::dump(void) const {

    std::cerr << "STATE: Id:  " << id << std::endl;
    std::cerr << "STATE: Pos: " << pos << std::endl;
    std::cerr << "STATE: Vel: " << vel << std::endl;
    std::cerr << "STATE: I:   " << current << std::endl;

    if (protocol == TMotorConstants::PROTOCOL_V2) {
        std::cerr << "STATE: Temperature: " << temperature << std::endl;
        std::cerr << "STATE: Error Core:  " << error << std::endl;
    }
}

}

}  // namespace libcubemars_can
