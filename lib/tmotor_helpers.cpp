//
// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#include <math.h>

namespace libcubemars_can {

int float_to_uint(float x, float x_min, float x_max, unsigned int bits) {
    float span = x_max - x_min;

    if ( x < x_min )
        x = x_min;
    else if ( x > x_max )
        x = x_max;

    return (int)((x-x_min)*((float)((1<<bits)-1)/span));

// C++ style cast, need to verify for equivalent functionality
//    return static_cast<int>( (x-x_min) * static_cast<float>((1 << bits)-1)/span );
}

float uint_to_float(int x, float x_min, float x_max, int bits) {
    float span = x_max - x_min;
    float offset = x_min;

    return ((float)x)*span/((float)((1<<bits)-1)) + offset;

// C++ style cast, need to verify for equivalent functionality
//    return static_cast<float>(x) * span/static_cast<float>((1 << bits)-1) + offset;
}

}  // namespace libcubemars_can
