//
// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo

#include <unistd.h>
#include <string.h>

#include <iostream>

#include "libcubemars_can/can_interface.h"

namespace libcubemars_can {


CanInterface::FactoryExpected CanInterface::Create(const std::string &device) {
    int fd;

// const int required_mtu = sizeof(struct can_frame);
// int mtu;
// int enable_canfd = 1;
    struct sockaddr_can addr;
    struct ifreq ifr;

    // open socket
    if ((fd = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
        FactoryError err("Error opening socket", errno);
        return tl::make_unexpected(err);
    }

    // Set socket timeout
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

    std::cerr << "Opening can device " << device << std::endl;
    strncpy(ifr.ifr_name, device.c_str(), IFNAMSIZ - 1);
    ioctl(fd, SIOCGIFINDEX, &ifr);

    memset(&addr, 0, sizeof(addr));
    addr.can_family = AF_CAN;
    addr.can_ifindex = ifr.ifr_ifindex;

    // if (required_mtu > (int)CAN_MTU) {

        /* check if the frame fits into the CAN netdevice */
        // if (ioctl(fd, SIOCGIFMTU, &ifr) < 0) {
        //     FactoryError err("SIOCGIFMTU error", errno);
        //     return tl::make_unexpected(err);
        // }
        // mtu = ifr.ifr_mtu;

        // if (mtu != CANFD_MTU) {
        // printf("CAN interface is not CAN FD capable - sorry.\n");
        // return 1;
        // }

        // /* interface is ok - try to switch the socket into CAN FD mode */
        // if (setsockopt(s, SOL_CAN_RAW, CAN_RAW_FD_FRAMES,
        //        &enable_canfd, sizeof(enable_canfd))){
        // printf("error when enabling CAN FD support\n");
        // return 1;
        // }

        // ensure discrete CAN FD length values 0..8, 12, 16, 20, 24, 32, 64
    // frame.len = can_fd_dlc2len(can_fd_len2dlc(frame.len));
    // }

    // disable default receive filter on this RAW socket
    // This is obsolete as we do not read from the socket at all, but for
    // this reason we can remove the receive list in the Kernel to save a
    // little (really a very little!) CPU usage.
    // setsockopt(s, SOL_CAN_RAW, CAN_RAW_FILTER, NULL, 0);

    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        FactoryError err("bind error", errno);
        return tl::make_unexpected(err);
    }

    return CanInterface(fd);
}


//== Member functions ==

CanInterface::CanInterface(int fd)
    : _fd(fd)
{;}

CanInterface::~CanInterface() {
    // if( _fd > 0 ) close(_fd);
}


int CanInterface::send(const struct can_frame &frame) {
    return write(_fd, &frame, sizeof(struct can_frame));
}


int CanInterface::receive(struct can_frame &frame) {
    return read(_fd, &frame, sizeof(struct can_frame));
}

int CanInterface::sendReceive(const struct can_frame &tx,
                              struct can_frame &rx) {
    auto s = send(tx);

    if ( s < 0 ) return s;

    return receive(rx);
}

}  // namespace libcubemars_can
